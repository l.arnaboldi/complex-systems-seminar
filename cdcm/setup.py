import setuptools

from cdcm  import (
  __pkgname__ as PKG_NAME,
  __author__  as AUTHOR
)

setuptools.setup(
  name = PKG_NAME,
  author  =  AUTHOR,
  packages = setuptools.find_packages(),
  python_requires = '>=3.10',
  install_requires = [
    'numpy',
    'scipy',
    'networkx',
    'pandas',
    'matplotlib',
    'seaborn',
    'tqdm'
  ]
)