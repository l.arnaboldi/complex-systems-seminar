import networkx as nx
import matplotlib.cm as cm
from cdcm import *

import numpy as np

N = 7
T = 10
a = np.random.normal(1.3,12,(N,T))
ts_a = TimeSeries(a)
Ca = ts_a.compute_correlation()
correlation_plot(Ca, filename = 'sample-correlation-matrix.pdf')

G = nx.Graph()
NCa = (Ca + 1.)/2
for i in range(N):
  for j in range(i+1,N):
    G.add_edge(i+1, j+1, color=cm.seismic(NCa[i][j]), weight = Ca[i][j])

pos = nx.circular_layout(G)

edges = G.edges()
colors = [G[u][v]['color'] for u,v in edges]
width = [5*abs(G[u][v]['weight']) for u,v in edges]


with plot_style():
    fig, ax = plt.subplots(figsize=(8,8))
    nx.draw(
      G,
      pos,
      edge_color=colors, 
      width=width,
      labels = {v+1:v+1 for v in range(N)},
      node_color = 'gainsboro',
      node_size = 1000.,
      font_size = 20
    )
    fig.savefig('sample-correlation-matrix-graph.pdf', bbox_inches = 'tight')

