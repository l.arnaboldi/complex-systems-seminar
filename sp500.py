from cdcm import *

import numpy as np

with open('datasets/sp500_10y.npy', 'rb') as f:
  sp500tickers = np.load(f)
  sp500_timeseries = np.load(f)

print(sp500_timeseries.shape)

N = sp500_timeseries.shape[0]
T = sp500_timeseries.shape[1]


ts_sp500 = TimeSeries(sp500_timeseries)
ts_sp500_shuffle = ts_sp500.shuffled()

C = ts_sp500.compute_correlation()
Csh = ts_sp500_shuffle.compute_correlation()
rmt_filter = RMTFilter(C)
rmt_filter_sh = RMTFilter(Csh)

print('Max eigenval.: ', max(rmt_filter.eigval))

## Eigenvalues plot
eigenvalues_plot(
  (rmt_filter.eigval, r'S\&P 500'),
  (rmt_filter_sh.eigval, r'Shuffled S\&P 500'),
  mp=True,
  N=N,
  T=T,
  xlim = (0.,3.7),
  ylim = 2.5,
  filename='figures/sp500/sp500_marcenko_pastur.pdf'
)

Cm = rmt_filter.extract_market_mode()
Cr = rmt_filter.extract_noise(T=T)
Cg = rmt_filter.extract_structure()

correlation_plot(C , filename=f'figures/sp500/sp500-C.png')
correlation_plot(Cm, filename=f'figures/sp500/sp500-Cm.png')
correlation_plot(Cr, filename=f'figures/sp500/sp500-Cr.png')
correlation_plot(Cg, filename=f'figures/sp500/sp500-Cg.png')

## Potts
# naive_potts = PottsMethod(C)
# naive_potts.fit(T0 = .05, steps = 50000, verbose = True, theta=-1.)
# energy_plot(naive_potts.steps, naive_potts.energies, filename=f'figures/sp500/sp500-naive-potts-energy-plot.png')

filtered_potts = PottsMethod(Cg)
filtered_potts.fit(T0 = .005, steps = 50000, verbose = True, theta=-10.,)
energy_plot(filtered_potts.steps, filtered_potts.energies, filename=f'figures/sp500/sp500-filtered-potts-energy-plot.png')
potts_comm = filtered_potts.find_communities()
print('Potts number of: ', len(np.unique(potts_comm)))
new_order = np.argsort(potts_comm)
correlation_plot(Cg[new_order, :][:,new_order], filename=f'figures/sp500/sp500-Cg-communities.png')

_, blocks = np.unique(potts_comm, return_counts=True)
correlation_plot(block_average(Cg[new_order, :][:,new_order], blocks), filename=f'figures/sp500/sp500-Cg-blockcommunities.png')

with open('sp500-communities.json', 'w') as f:
  results = {t:str(c) for t,c in zip(sp500tickers, potts_comm)}
  import json
  json.dump(results , f, indent=2)

# Random Guess
print('Random guess: ', shared_information_distance(np.random.randint(0,len(np.unique(potts_comm)), size=(len(potts_comm),)), potts_comm))

# Asset Graph
filtered_ag = AssestGraph(Cg)
filtered_ag.estimate_threshold(T)
filtered_ag.fit()
ag_comm = filtered_ag.find_communities()
print('AG number of: ', len(np.unique(ag_comm)))
ag_new_order = np.argsort(ag_comm)
correlation_plot(Cg[ag_new_order, :][:,ag_new_order], filename=f'figures/sp500/sp500-Cg-AGcommunities.png')
print(f'AG VI: {shared_information_distance(ag_comm, potts_comm)}')

# MST
filtered_mst = MinimumSpanningTree(Cg)
filtered_mst.fit()
mst_comm = filtered_mst.find_communities(3)
mst_new_order = np.argsort(mst_comm)
correlation_plot(Cg[mst_new_order, :][:,mst_new_order], filename=f'figures/sp500/sp500-Cg-MSTcommunities.png')
print(f'MST VI: {shared_information_distance(mst_comm, potts_comm)}')