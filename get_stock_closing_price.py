import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def from_ticker_list(tickers_list, duration = '10y'):
  import yfinance as yf
  data = yf.download(tickers_list,period=duration)['Adj Close']
  # print(data)
  data.dropna(axis=1,inplace=True)
  # print(data)
  price_series = data.to_numpy().T
  # print(price_series)
  log_return_series = np.log(price_series[:,1:])-np.log(price_series[:,:-1])
  print('Original number of tickers:',len(tickers_list), '. Number of cleaned tickers', len(log_return_series))
  print('Number of days: ', log_return_series.shape)
  # plt.plot(range(len(log_return_series[0])), log_return_series.T, label=tickers_list)
  # plt.legend()
  # plt.show()

  return log_return_series

import csv
with open('datasets/sp500.csv', 'r') as f:
    reader = csv.DictReader(f)
    sp500tickers = []
    for row in reader:
      sp500tickers.append(row['Symbol'])


FTSEMIBtickers = [
  "A2A.MI", 
  "AMP.MI",
  "ATL.MI",
  "AZM.MI",
  "BGN.MI",
  "BMED.MI",
  "BAMI.MI",
  "BPE.MI",
  "CPR.MI",
  "CNHI.MI",
  "DIA.MI",
  "ENEL.MI",
  "ENI.MI",
  "EXO.MI",
  "RACE.MI",
  "FBK.MI",
  "G.MI",
  "HER.MI",
  "IP.MI",
  "ISP.MI",
  "INW.MI",
  "IG.MI",
  "IVG.MI",
  "LDO.MI",
  "MB.MI",
  "MONC.MI",
  "NEXI.MI",
  "PIRC.MI",
  "PST.MI",
  "PRY.MI",
  "REC.MI",
  "SPM.MI",
  "SPMAA.MI",
  "SRG.MI",
  "STLA.MI",
  "STM.MI",
  "TIT.MI", 
  "TEN.MI",
  "TRN.MI", 
  "UCG.MI",
]

with open('datasets/ftseMIB40_3y.npy', 'wb') as f:
  np.save(f, np.array(FTSEMIBtickers))
  np.save(f, from_ticker_list(FTSEMIBtickers, '3y'))

with open('datasets/sp500_10y.npy', 'wb') as f:
  np.save(f, np.array(sp500tickers))
  np.save(f, from_ticker_list(sp500tickers))